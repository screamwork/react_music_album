import React, { Component } from 'react';
import { Link, Route } from 'react-router-dom';
import AlbumsPage from './AlbumsPage';
import AlbumForm from './AlbumForm';
import ModalRoot from './ModalRoot';


class App extends Component {
  render() {
    return (
      <div className="ui container">
        <div className="ui two item menu">
          <Link className="item" to="/albums">Albums</Link>
          <Link className="item" to="/albums/new">Add Album</Link>
        </div>

        <Route exact path="/albums" component={AlbumsPage} />
        <Route path="/albums/new" component={AlbumForm} />
        <ModalRoot />
      </div>
    );
  }
}

export default App;
