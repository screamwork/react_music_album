import React from 'react';
import AlbumCard from './AlbumCard';


class AlbumsList extends React.Component {

  render() {
    const emptyMessage = (
      <p>There are no albums yet.</p>
    );

    const albumsList = (
      <div className="ui six cards">
        { this.props.albums.map(album => <AlbumCard album={album} key={album.id} />) }
      </div>
    )

    return (
      <div>
        { this.props.albums.length === 0 ? emptyMessage : albumsList }
      </div>
    );
  }
}

AlbumsList.propTypes = {
  albums: React.PropTypes.array.isRequired,
}

export default AlbumsList;
