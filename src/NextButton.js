import React from 'react';
import { connect} from 'react-redux';
import { handlePagination } from './actions';

class NextButton extends React.Component {

  handleNextClick(e) {
    this.props.handlePagination('next', this.props.next);
  }

  render() {
    return (
      <button className="ui button" onClick={this.handleNextClick.bind(this)} disabled={!!this.props.next ? '' : 'disabled'}>
      Next
      </button>
    );
  }
}

export default connect(null, { handlePagination })(NextButton)
