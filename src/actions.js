// import WPAPI from 'wpapi';
export const SET_ALBUMS = 'SET_ALBUMS';
export const SET_TAGS = 'SET_TAGS';
export const ADD_ALBUM = 'ADD_ALBUM';
export const SHOW_MODAL = 'SHOW_MODAL';
export const HIDE_MODAL = 'HIDE_MODAL';
export const SET_INITIAL_PAGINATION = 'SET_INITIAL_PAGINATION';
export const SET_PAGINATION = 'SET_PAGINATION';
export const SET_NEXT_PAGINATION = 'SET_NEXT_PAGINATION';
export const SET_PREV_PAGINATION = 'SET_PREV_PAGINATION';
export const DISABLE_PAGINATION = 'DISABLE_PAGINATION';

export const PER_PAGE = 30;


function handleResponse(response) {
  if(response.ok) {
    return response.json();
  } else {
    let error = new Error(response.statusText);
    error.response = response;
    throw error;
  }
}

export function setAlbums(albums) {
  return {
    type: SET_ALBUMS,
    albums
  }
}

export function addAlbum(album) {
  return {
    type: ADD_ALBUM,
    album
  }
}

export function saveGame(data) {
  return dispatch => {
    let genre = data.genre;
    if(!Array.isArray(genre))
      genre = genre.split(',');
    let post = {
      title: data.title,
      cover: data.cover,
      genre,
      songs: data.songs
    }
    return fetch('http://localhost/wp_test_new/wp-json/music-album/v1/add-album', {
      method: 'post',
      body: JSON.stringify(post),
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Basic c2N3OmluZGlheHg=",
      }
    }).then(handleResponse)
      .then(data => {
        fetch('http://localhost/wp_test_new/wp-json/wp/v2/music-album/' + data)
        .then(handleResponse)
        .then(data => {
          dispatch(addAlbum(data));
        });
      });
  }
}

export function fetchAlbums(page) {
  return dispatch => {
    fetch('http://localhost/wp_test_new/wp-json/wp/v2/music-album?per_page=' + PER_PAGE + '&page=' + page)
      .then(res => {
        dispatch(
          setPagination(
            parseInt(res.headers.get('X-WP-Total'), 10),
            parseInt(res.headers.get('X-WP-TotalPages'), 10)
          )
        );
        return res.json();
      })
      .then(data => {
        dispatch( setAlbums(data) );
      })


  }
}

export function getTags() {
  return dispatch => {
    fetch('http://localhost/wp_test_new/wp-json/wp/v2/music_genre?per_page=100')
      .then(res => res.json())
      .then(data => {
        dispatch( setTags(data) );
      });
  }
}

export function setTags(tags) {
  return {
    type: SET_TAGS,
    tags
  }
}

export function changedSelectedTag(tag, page = 1) {
  return dispatch => {
    if(tag === "") {
      dispatch(fetchAlbums(1))
    } else {
      fetch('http://localhost/wp_test_new/wp-json/wp/v2/music-album?music_genre=' + tag + '&page=' + page)
      .then(res => {
        dispatch(setInitialPagination())
        dispatch(
          setPagination(
            parseInt(res.headers.get('X-WP-Total'), 10),
            parseInt(res.headers.get('X-WP-TotalPages'), 10)
          )
        );
        return res.json()
      })
      .then(data => {
        dispatch( setAlbums(data) );
      });
    }
  }
}

export function showAlbumModal(modalType, modalProps) {
  return dispatch => {
    dispatch(showModal(
      modalType,
      modalProps
    ))
  }
}

export function showModal(modalType, modalProps) {
  return {
    type: SHOW_MODAL,
    modalType,
    modalProps
  }
}

export function hideModal() {
  return {
    type: HIDE_MODAL
  }
}

export function setInitialPagination() {
  return {
    type: SET_INITIAL_PAGINATION,
  }
}

export function setPagination(total, pages) {
  return {
    type: SET_PAGINATION,
    total,
    pages
  }
}

export function buttonNextClick() {
  return {
    type: SET_NEXT_PAGINATION
  }
}

export function buttonPrevClick() {
  return {
    type: SET_PREV_PAGINATION
  }
}

export function unmountPagination() {
  return {
    type: DISABLE_PAGINATION
  }
}

export function handlePagination(button, page) {
  return dispatch => {
    if(button === 'next') {
      dispatch(buttonNextClick());
    }
    else if(button === 'prev') {
      dispatch(buttonPrevClick());
    }
    dispatch(fetchAlbums(page));
  }
}



// .headers({
//   "Cross-Domain": true,
//   "Content-Type": "application/json"
// })
// export function saveGame(data) {
//   return dispatch => {
//     var wp = new WPAPI({
//         endpoint: 'http://localhost/wp_test_new/wp-json/',
//         // This assumes you are using basic auth, as described further below
//         username: 'scw',
//         password: 'indiaxx'
//     });
//     return wp.posts().create({
//         // "title" and "content" are the only required properties
//         title: data.title,
//         content: data.cover,
//         // Post will be created as a draft by default if a specific "status"
//         // is not specified
//         status: 'publish'
//     })
//     .then(handleResponse)
//   }
// }
