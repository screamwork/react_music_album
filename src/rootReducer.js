import { combineReducers } from 'redux';

import albums from './reducers/albums';
import tags from './reducers/tags';
import modal from './reducers/modal';
import pagination from './reducers/pagination';

export default combineReducers({
  albums,
  tags,
  modal,
  pagination
});
