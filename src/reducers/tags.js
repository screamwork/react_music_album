import { SET_TAGS } from '../actions';

export default function tags(state = [], action = {}) {
  switch(action.type) {

    case SET_TAGS:
      return action.tags;

    default: return state;
  }
}
