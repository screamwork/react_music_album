import { SET_ALBUMS, ADD_ALBUM } from '../actions';

export default function albums(state = [], action = {}) {
  switch(action.type) {

    case SET_ALBUMS:
      return action.albums;

    case ADD_ALBUM:
      return [
        ...state,
        action.album
      ]


    default: return state;
  }
}
