import { SET_INITIAL_PAGINATION, SET_NEXT_PAGINATION, SET_PREV_PAGINATION, DISABLE_PAGINATION, SET_PAGINATION } from '../actions';

let initialState = {
  total: null,
  pages: null,
  current: 1,
  next: null,
  prev: null
}

export default function pagination(state = {}, action = {}) {
  switch (action.type) {

    case DISABLE_PAGINATION:
      initialState.total = null;
      initialState.pages = null;
      initialState.next = null;
      initialState.prev = null;
      initialState.current = null;
      return initialState;

    case SET_INITIAL_PAGINATION:
        initialState.total = null;
        initialState.pages = null;
        initialState.next = null;
        initialState.prev = null;
        initialState.current = 1;
        return initialState;

    case SET_PAGINATION:
      initialState.total = action.total;
      initialState.pages = action.pages;
      initialState.next = (initialState.current + 1) > initialState.pages ? null : initialState.current + 1;
      initialState.prev = (initialState.current - 1) > 0  ? initialState.current - 1 : null;

      return initialState;

    case SET_NEXT_PAGINATION:
      initialState.current = (initialState.next !== null ? ++initialState.current : initialState.current);
      initialState.next = ( (initialState.current + 1) >= initialState.pages ) ? null : initialState.current + 1;
      return initialState;

    case SET_PREV_PAGINATION:
      initialState.current = (initialState.prev !== null ? --initialState.current : initialState.current);
      initialState.prev = ( (initialState.current - 1) > 1 )  ? initialState.current - 1 : null;
      return initialState;

    default:
      return state
  }
}
