import React from 'react'
import { hideModal } from './actions'
import { connect } from 'react-redux'

const AlbumModal = ({ album, dispatch }) => (
  <div className="ui dimmer modals page transition visible active">
    <div className="ui standard modal transition visible active scrolling">
      <div className="header">
        <div className="header">{album.title_text}</div>
      </div>
      <div className="image content">
        <div className="ui medium image">
          <img src={album.cover} alt="cover" />
        </div>
        <div className="description">
          <div className="ui header">Songs</div>
            { album.content.rendered !== ""
              ? <div className="body">{ album.content.rendered }</div>
              : <div className="body"><ol>{ album.tracklist.map( (track, i) => <li key={i}>{track}</li> ) }</ol></div>
            }
          <div className="ui header">Genre</div>
          <div className="body">
            <ul className="genres">
            { album.music_genre_text.map( (genre, idx) => <li key={idx} className="genre">{genre}</li> ) }
            </ul>
          </div>
        </div>
      </div>
      <div className="actions">
        <div className="ui black deny button" onClick={ () => dispatch(hideModal()) }>
          Close
        </div>
      </div>
    </div>
  </div>
)

export default connect(null)(AlbumModal)
