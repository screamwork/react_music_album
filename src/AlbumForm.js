import React from 'react';
import classnames from 'classnames';
import { connect } from 'react-redux';
import { saveGame } from './actions';
import { Redirect } from 'react-router-dom';
import axios from 'axios';

class AlbumForm extends React.Component {
  state = {
    title: '',
    cover: '',
    genre: [],
    songs: [],
    errors: {},
    loading: false,
    done: false
  }

  componentWillUnmount() {
    this.setState({
      title: '',
      cover: '',
      genre: [],
      songs: [],
      errors: {},
    });
  }

  fetchAlbum(e) {
    if(e.target.value !== '') {
      let name = e.target.value.split('-');
      name[0] = name[0].trim();
      name[1] = name[1].trim();
      if(name.length === 2) {
        const url = `http://ws.audioscrobbler.com/2.0/?method=album.getinfo&api_key=216ba84a21b975f2873c68cee721a0b1&artist=${name[0]}&album=${name[1]}&format=json`;

        axios.get(url)
        .then(response => {
          if(response.data.album) {
            const { album } = response.data
            let tags = [];
            for(let tag of album.tags.tag) {
              tags.push(tag.name);
            }
            const image = album.image[3]['#text'];
            let tracks = [];
            for(let track of album.tracks.track) {
              tracks.push(track.name);
            }

            this.setState({
              cover: image,
              genre: tags,
              songs: tracks
            });
          }
        }, (err) => {
          let errors = {
            global: err.toString()
          };
          this.setState({ errors, loading: false });
        });
      }
    }
  }

  handleChange = (e) => {
    if(!!this.state.errors[e.target.name]) {
      let errors = Object.assign({}, this.state.errors);
      delete errors[e.target.name];
      this.setState({
        [e.target.name]: e.target.value,
        errors
      });
    } else {
      this.setState({ [e.target.name]: e.target.value});
    }
  }

  handleSubmit = (e) => {
    e.preventDefault();

    // validation
    let errors = {};
    if (this.state.title === '') errors.title = "Can't be empty!";
    if (this.state.cover === '') errors.cover = "Can't be empty!";
    if (this.state.genre === '') errors.genre = "Can't be empty!";
    if (this.state.songs === '') errors.songs = "Can't be empty!";
    this.setState({ errors });
    const isValid = Object.keys(errors).length === 0;

    if(isValid) {
      const { title, cover, genre, songs } = this.state;
      this.setState({ loading: true });
      this.props.saveGame({ title, cover, genre, songs }).then(
        () => { this.setState({ done: true }) },
        (err) => {
          let errors = {
            global: err.toString()
          };
          this.setState({ errors, loading: false });
        }
      );
    }
  }

  render() {
    const form = (
      <form className={classnames('ui', 'form', { loading: this.state.loading })} onSubmit={this.handleSubmit} >
        <h1>Add Album</h1>

        { !!this.state.errors.global && <div className="ui negative message"><p>{this.state.errors.global}</p></div> }

        <div className={classnames('field', { error: !!this.state.errors.title })}>
          <label htmlFor="title">Title</label>
          <input
            name="title"
            value={this.state.title}
            onChange={this.handleChange}
            onBlur={this.fetchAlbum.bind(this)}
            id="title"
          />
        </div>
        <span>{this.state.errors.title}</span>

        <div className={classnames('field', { error: !!this.state.errors.cover })}>
          <label htmlFor="cover">Cover URL</label>
          <input
            name="cover"
            value={this.state.cover}
            onChange={this.handleChange}
            id="cover"
          />
        </div>
        <span>{this.state.errors.cover}</span>

        <div className={classnames('field', { error: !!this.state.errors.genre })}>
          <label htmlFor="genre">Genre</label>
          <input
            name="genre"
            value={this.state.genre}
            onChange={this.handleChange}
            id="genre"
          />
        </div>
        <span>{this.state.errors.genre}</span>

        <div className="field">
          { this.state.cover !== '' && <img src={this.state.cover} alt="" className="ui small bordered image" /> }
        </div>

        <div className={classnames('field', { error: !!this.state.errors.songs })}>
          <label htmlFor="songs">Songs</label>
          <textarea
            rows="2"
            name="songs"
            value={this.state.songs}
            onChange={this.handleChange}
            id="songs"
          ></textarea>
        </div>
        <span>{this.state.errors.songs}</span>

        <div className="field">
          <button className="ui primary button">Save</button>
        </div>

      </form>
    )
    return (
      <div>
        { this.state.done ? <Redirect to="/albums" /> : form }
      </div>
    );
  }
}

export default connect(null, { saveGame })(AlbumForm);
