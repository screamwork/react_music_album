import React from 'react';
import { connect} from 'react-redux';
import { handlePagination } from './actions';

class PrevButton extends React.Component {

  handlePrevClick(e) {
    this.props.handlePagination('prev', this.props.prev);
  }

  render() {
    return (
      <button className="ui button" onClick={this.handlePrevClick.bind(this)} disabled={!!this.props.prev ? '' : 'disabled'}>
      Previous
      </button>
    );
  }
}

export default connect(null, { handlePagination })(PrevButton)
