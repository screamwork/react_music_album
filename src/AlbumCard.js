import React from 'react'
import { showAlbumModal } from './actions'
import { connect } from 'react-redux'


class AlbumCard extends React.Component {
  constructor() {
    super()
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick(e) {
    this.props.showAlbumModal(
      'ALBUM_POST',
      { album: this.props.album }
    )
  }

  render() {
    let titles = this.props.album.title_text.split("-")
    titles = titles.map(string => string.trim())
    
    return (
      <div className="ui card" onClick={this.handleClick}>
        <div className="image">
          <img src={this.props.album.cover} alt="cover" />
        </div>
        <div className="content">
          <div className="header">{titles[1]}</div>
          <div className="description">{titles[0]}</div>
        </div>
      </div>
    );
  }
}

AlbumCard.propTypes = {
  album: React.PropTypes.object.isRequired,
  showAlbumModal: React.PropTypes.func.isRequired,
}

export default connect(null, {showAlbumModal})(AlbumCard)
