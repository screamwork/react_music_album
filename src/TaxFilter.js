import React from 'react';
import { connect } from 'react-redux';
import { changedSelectedTag } from './actions';

class TaxFilter extends React.Component {
    constructor() {
        super();
        this.state = {
          childSelectValue: ""
        };
        this.changeHandler = this.changeHandler.bind(this);
    }
    changeHandler(e) {
        this.setState({
            childSelectValue: e.target.value
        });
        this.props.changedSelectedTag(e.target.value);
    }
    render() {
        var createOptions = this.props.tags.map((tag, i) => {
            return (
                <option key={i} value={tag.id}>{tag.name}</option>
            );
        });
        return (
            <select
              className="ui search dropdown"
              value={this.state.childSelectValue}
              onChange={this.changeHandler}
            >
              <option value="">Genre (all)</option>
              {createOptions}
            </select>
        )
    }
};

TaxFilter.propTypes = {
  tags: React.PropTypes.array.isRequired,
}

export default connect(null, { changedSelectedTag })(TaxFilter);
