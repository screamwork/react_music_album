import React from 'react';
import { connect } from 'react-redux';
import AlbumsList from './AlbumsList';
import { fetchAlbums, getTags, setInitialPagination, unmountPagination } from './actions';
import TaxFilter from './TaxFilter';
import PrevButton from './PrevButton';
import NextButton from './NextButton';


class AlbumsPage extends React.Component {

  componentWillMount() {
    this.props.setInitialPagination();
  }
  componentWillUnmount() {
    this.props.unmountPagination();
  }

  componentDidMount() {
    this.props.fetchAlbums(1);
    this.props.getTags();
  }

  render() {
    return (
      <div>
        <h1>AlbumsList</h1>
        <div className="ui equal width grid">
          <div className="column">
            <TaxFilter tags={this.props.tags} />
          </div>
          <div className="column right aligned">
            <PrevButton  prev={this.props.pagination.prev} />
            <NextButton next={this.props.pagination.next} />
          </div>
        </div>
        <AlbumsList albums={this.props.albums} />
      </div>
    );
  }
}

AlbumsPage.propTypes = {
  albums: React.PropTypes.array.isRequired,
  tags: React.PropTypes.array.isRequired,
  fetchAlbums: React.PropTypes.func.isRequired,
  getTags: React.PropTypes.func.isRequired,
  setInitialPagination: React.PropTypes.func.isRequired,
  unmountPagination: React.PropTypes.func.isRequired,
}

function mapStateToProps(state) {
  return {
    albums: state.albums,
    tags: state.tags,
    pagination: state.pagination
  }
}

export default connect(mapStateToProps, { fetchAlbums, getTags, setInitialPagination, unmountPagination })(AlbumsPage);
