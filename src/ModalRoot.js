import React from 'react';
import AlbumModal from './AlbumModal';
import { connect } from 'react-redux';

const MODAL_COMPONENTS = {
  'ALBUM_POST': AlbumModal
}

const ModalRoot = ({ modalType, modalProps }) => {

  if (!modalType) {
    return null // after React v15 you can return null here
  }

  const SpecificModal = MODAL_COMPONENTS[modalType]
  return <SpecificModal {...modalProps} />
}

export default connect( state => state.modal )(ModalRoot);
